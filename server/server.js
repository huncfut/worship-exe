const ws = require('ws')
const env = require('./env')

const server = ws.Server({
  port: env.PORT
})

server.serve('forever')
