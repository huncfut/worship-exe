var canvas, ctx
var n = 0


window.onload = () => {
  canvas = document.getElementById('canvas')
  canvas.width = document.documentElement.clientWidth
  canvas.height = document.documentElement.clientHeight
  ctx = canvas.getContext('2d')
  document.addEventListener('keydown', handleKeyDown)
  ctx.font = "30px Arial";
  ctx.textAlign = "center";
  draw()
}

window.onresize = () => {
  canvas.width = document.documentElement.clientWidth
  canvas.height = document.documentElement.clientHeight
  ctx.font = "30px Arial";
  ctx.textAlign = "center";
  draw()
}

const handleKeyDown = e => {
  switch (e.keyCode) {
    case KEYCODE_ARROW_LEFT:
    case KEYCODE_ARROW_UP:
      n--
      draw()
      break
    case KEYCODE_ARROW_RIGHT:
    case KEYCODE_ARROW_DOWN:
      n++
      draw()
      break
  }
}
var song = {
  title: 'Otworz me oczy',
  parts: {
    v1: ["Otwórz me oczy o Panie", "Otwórz me oczy i serce", "Chcę widziec Ciebie", "Chcę widziec Ciebie"],
    c: ["Wywyższonego widziec chce", "Ujrzec Ciebie w blasku twej chwaly", "Wylej swa milosc i moc", "gdy spiewam Swiety, Swiety, Swiety"],
    o: ["Swiety, Swiety, Swiety", "Swiety, Swiety, Swiety", "Swiety, Swiety, Swiety", "Chce widziec Ciebie"]
  },
  order: ['v1', 'v1', 'c', 'v1', 'c', 'c', 'o']
}

const draw = () => {
  ctx.fillStyle = 'rgb(0, 0, 0)'
  ctx.fillRect(0, 0, canvas.width, canvas.height)
  ctx.fillStyle = 'rgb(255, 255, 255)'
  ctx.fillText((n < pres.length) ? pres[n] : '', canvas.width/2, 100)
}

const createLines = (song) => {
  return song.order.map(part => song.parts[part]).flat(1)
}

const pres = createLines(song)
